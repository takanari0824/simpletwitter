package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class UserDao {

	public User select(Connection connection, String accountOrEmail, String password) {
//		引数にはconnection, フォームから受け取った照合データ（accountOrEmail, password）

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE (account = ? OR email = ?) AND password = ?";
//			バインド変数?を使用してSQL文を作成

			ps = connection.prepareStatement(sql);
//			connectionのprepareStatementメソッドにバインド変数を含むSQL文の枠組みを渡す


			ps.setString(1,  accountOrEmail);
			ps.setString(2,  accountOrEmail);
			ps.setString(3, password);
//			sqlの?の部分の値をセットして使える状態にする
//			setString(?の位置番号, 代入する値)

			ResultSet rs = ps.executeQuery();
//			SQL文の実行結果をrsで受け取る

			List<User> users = toUsers(rs);
//			SQL文で受けとったデータをtoUsersメソッドを利用してList<User>に変換する

			if(users.isEmpty()) {
//				SQL文で何も受け取れてなかったとき
				return null;
			} else if(2 <= users.size()) {
//				複数のデータを受け取ったとき
				throw new IllegalStateException("ユーザーが重複しています");
			} else {
//				1件のデータを受け取ったとき
				return users.get(0);
//				リストに格納されている1件のUserインスタンスをreturnする
			}

		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User select(Connection connection, int id) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);

			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			List<User> users = toUsers(rs);
			if(users.isEmpty()) {
				return null;
			} else if (2 <= users.size()) {
				throw new IllegalStateException("ユーザーが重複しています");
			} else {
				return users.get(0);
			}
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User accountCheckSelect(Connection connection, String account) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE account = ?";

			ps = connection.prepareStatement(sql);

			ps.setString(1, account);

			ResultSet rs = ps.executeQuery();

			List<User> users = toUsers(rs);
			if (1 <= users.size()) {
				return users.get(0);
			} else {
				return null;
			}
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append(" account, ");
			sql.append(" name, ");
			sql.append(" email, ");
			sql.append(" password, ");
			sql.append(" description, ");
			sql.append(" created_date, ");
			sql.append(" updated_date ");
			sql.append(") VALUES (");
			sql.append(" ?, ");
			sql.append(" ?, ");
			sql.append(" ?, ");
			sql.append(" ?, ");
			sql.append(" ?, ");
			sql.append(" CURRENT_TIMESTAMP, ");
			sql.append(" CURRENT_TIMESTAMP");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getName());
			ps.setString(3, user.getEmail());
			ps.setString(4, user.getPassword());
			ps.setString(5,  user.getDescription());

			ps.executeUpdate();
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, User user) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("UPDATE users SET ");
	        sql.append("account = ?, ");
	        sql.append("name = ?, ");
	        sql.append("email = ?, ");
//	        パスワードも変更するとき
	        if(!StringUtils.isBlank(user.getPassword())) {
	        	sql.append("password = ?,");
	        }
	        sql.append("description = ?, ");
	        sql.append("updated_date = CURRENT_TIMESTAMP ");
	        sql.append("WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getName());
			ps.setString(3, user.getEmail());
//			パスワードも変更するとき
			if(!StringUtils.isBlank(user.getPassword())) {
				ps.setString(4, user.getPassword());
				ps.setString(5, user.getDescription());
				ps.setInt(6,  user.getId());
//			パスワードは変更しないとき
			} else {
				ps.setString(4, user.getDescription());
				ps.setInt(5,  user.getId());
			}


			int count = ps.executeUpdate();
			if(count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


//	selectで受けとりrsに格納されたデータをList型にして返すメソッド
	private List<User> toUsers(ResultSet rs) throws SQLException {
		List<User> users = new ArrayList<User>();
		try {
			while(rs.next()) {
//				該当するデータがあった場合にユーザーインスタンスにデータを格納していく
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setAccount(rs.getString("account"));
				user.setName(rs.getString("name"));
				user.setEmail(rs.getString("email"));
				user.setPassword(rs.getString("password"));
				user.setDescription(rs.getString("description"));
				user.setCreatedDate(rs.getTimestamp("created_date"));
				user.setUpdatedDate(rs.getTimestamp("updated_date"));

//				値を入れ終わったインスタンスをリストに格納
				users.add(user);
			}
			return users;
		} finally {
//			使用したrsはクローズする
			close(rs);
		}
	}

}
