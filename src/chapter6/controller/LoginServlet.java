package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/login"})
public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		String accountOrEmail = request.getParameter("accountOrEmail");
		String password = request.getParameter("password");
//		requestのパラメータからフォームで指定したname(キー)で値を受け取る

		User user = new UserService().select(accountOrEmail, password);
//		フォームから受け取ったデータ（acccountOrEmail, password）を基に
//		該当ユーザー情報がデータベースにあるかどうか確認

//		データベースに該当のユーザー情報がなかった時の処理
		if( user == null) {
//			エラー文を作成してフォワード先に渡す
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("ログインに失敗しました");
			request.setAttribute("errorMessages",  errorMessages);
			request.getRequestDispatcher("login.jsp").forward(request,  response);
			return;
		}

//		該当のユーザー情報があったときはログイン状況を示すセッションを作成
		HttpSession session = request.getSession();
//		セッションの中にログイン中のユーザーの情報セット
		session.setAttribute("loginUser", user);
		response.sendRedirect("./");
	}

}
