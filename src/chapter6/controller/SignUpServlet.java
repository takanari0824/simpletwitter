package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/signup"})
public class SignUpServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		request.getRequestDispatcher("signup.jsp").forward(request,  response);
//		getメソッドの時(signup画面を開いたとき)には特に処理は行わず、ビューの表示をフォワード先に任せる
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		List<String> errorMessages = new ArrayList<String>();
//		requestで送られてくるフォームの内容に不備があったときに、エラー文を入れる箱

		User user = getUser(request);
//		getUserメソッドを使用してUserクラスのインスタンス作成

		if(!isValid(user, errorMessages)) {
//			isValidメソッドの実行結果がfalseだった時の処理
			request.setAttribute("errorMessages", errorMessages);
//			フォワード先でエラーメッセージの一覧にアクセスできるようにsetAttribute();
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}

		new UserService().insert(user);
		response.sendRedirect("./");
//		フォームの送信の重複が起こらないように、PRGパターンを利用
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {
		User user = new User();
		user.setName(request.getParameter("name"));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setEmail(request.getParameter("email"));
		user.setDescription(request.getParameter("description"));
//		フォームの内容が格納されたrequestからキーを指定してuserの情報をsetしていく
		return user;
	}

	private boolean isValid(User user, List<String> errorMessages) {
		String name = user.getName();
		String account = user.getAccount();
		String password = user.getPassword();
		String email = user.getEmail();
//		getUserで情報を格納したUserインスタンスをもとにデータチェック

		if(!StringUtils.isEmpty(name) && (20 < name.length())) {
			errorMessages.add("名前は20文字以下で入力してください");
		}

		if(StringUtils.isEmpty(account)) {
			errorMessages.add("アカウント名を入力してください");
		} else if(20 < account.length()) {
				errorMessages.add("アカウント名は20文字以下で入力してください");
		}

		if(sameAccountExist(account)) {
			errorMessages.add("他ユーザーとアカウント名が重複しています。");
		}

		if(StringUtils.isEmpty(password)) {
			errorMessages.add("パスワードを入力してください");
		}

		if(!StringUtils.isEmpty(email) && (50 < email.length())) {
			errorMessages.add("メールアドレスは50文字以下で入力してください");
		}

		if(errorMessages.size() != 0) {
			return false;
//			1つ以上のエラーがある場合にはfalseを返すメソッド
		}
		return true;
//		1つ前のIf分を通っていない = errorMessages.size() == 0 = フォーム内容に誤りなし
	}

	private boolean sameAccountExist(String account) {
		User user = new UserService().accountCheckSelect(account);
		if( user != null) {
			return true;
		}
		return false;
	}

}
