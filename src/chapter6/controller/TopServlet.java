package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.User;
import chapter6.beans.UserMessage;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = {"/index.jsp"})
public class TopServlet extends HttpServlet {
	private static final long serialVertionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		boolean isShowMessageForm = false;
//		フォーム画面を出すかどうかのデフォルト値はfalse（出さない）

		User user = (User) request.getSession().getAttribute("loginUser");
//		ログイン時にsetAttributeしたsessionからloginUserを取得する
		if(user != null) {
			isShowMessageForm = true;
//			loginuserがnullでなければ(ログインしていれば)trueを返す =  フォーム出す
		}

		List<UserMessage> messages = null;
		if(request.getParameter("user_id") == null) {
			messages = new MessageService().select();
		} else {
			int userId = Integer.parseInt(request.getParameter("user_id"));
			messages = new MessageService().selectWithUserId(userId);
		}

		request.setAttribute("messages", messages);
		request.setAttribute("isShowMessageForm", isShowMessageForm);
		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}

}
